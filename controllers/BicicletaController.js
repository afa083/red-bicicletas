const { request } = require('../app');
var Bicicleta = require('../models/bicicleta');

exports.Bicicleta_List = function (req, res) {
    res.render('Bicicleta/index', { bicicletas: Bicicleta.allbicis });
}

exports.Bicicleta_Create_GET = function (req, res) {
    res.render('Bicicleta/create');
}

exports.Bicicleta_Create_POST = function (req, res) {
    var bici = new Bicicleta (req.body.id,req.body.color,req.body.modelo);
    bici.ubicacion =[req.body.lat,req.body.lon];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.Bicicleta_Update_GET = function (req, res) {
    var bici = new Bicicleta.findById(req.params.id);
    res.render('Bicicleta/update', {bici});
}

exports.Bicicleta_Update_POST = function (req, res) {
    var bici = new Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat,req.body.lon];
    res.redirect('/bicicletas');
}

exports.Bicicleta_delete_POST = function (req, res) {
    Bicicleta.removeById(req.body.id);

    res.render('bicicletas/');
}