var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){

  res.status(200).json({Bicicletas:Bicicleta.allbicis});
}

exports.bicicleta_create = function(req,res){
  var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
    
      bici.ubicacion = [req.body.lat,req.body.lon];
  res.status(200).json({Bicicleta:bici});

  Bicicleta.add(bici);
}

exports.bicicleta_delete = function(req,res){
  Bicicleta.removeById(req.body.id);
  res.status(204).send;

}