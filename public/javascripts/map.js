var mymap = L.map('mapid').setView([3.61083,-76.2698709], 18);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWZhMDgzIiwiYSI6ImNrZ2ozdDl1bzBtaWMyeG8zM2N2emxyYngifQ.tC5a6imK3JXo4b_spNouKw'
}).addTo(mymap);


$.ajax({

    dataType: "json",
    url:"/api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach[function(bici){
            L.marker(bici.ubicacion,{title:bici.id}).addTo(mymap);
            
        }];
            
       
    }
})