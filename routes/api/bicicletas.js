var express = require('express');
var router = express.Router();
var BicicletaControllerApi = require('../../controllers/api/BicicletaControllerApi');

router.get('/', BicicletaControllerApi.bicicleta_list);
router.post('/create', BicicletaControllerApi.bicicleta_create);
router.delete('/delete', BicicletaControllerApi.bicicleta_delete);


module.exports = router;