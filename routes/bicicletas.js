var express = require('express');
var router = express.Router();
var BicicletaController = require('../controllers/BicicletaController');


router.get('/', BicicletaController.Bicicleta_List);
router.get('/create', BicicletaController.Bicicleta_Create_GET);
router.post('/create', BicicletaController.Bicicleta_Create_POST);
router.get('/:id/update', BicicletaController.Bicicleta_Update_GET);
router.post('/:id/update', BicicletaController.Bicicleta_Update_POST);
router.post('/:id/delete', BicicletaController.Bicicleta_delete_POST);
  
  
module.exports = router;