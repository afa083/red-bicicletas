var Bicicleta = require('../../models/bicicleta');
const request = require('request');
var rserver = require ('../../bin/www');

describe("Bicicletas Api", function() {
    describe("GET_Bicicletas", function() {
     it("status 200", function() {
        var a =  new Bicicleta (1 ,'verde', 'Todoterreno',[3.61068, -76.27099]);
        Bicicleta.add(a);
        request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
            expect(response.statusCode).toBe(200);
        });
      
       });
   });
  });

describe("POST BICICLETAS /create", function() {
    it("status 200", function(done) {
      var headers ={ 'content-type' : 'applicacion/json'};
      var bici =  ' {"id": 1 ,"color": "verde", "modelo" :"Todoterreno","lat": 3.61068, "lon": -76.27099}';
      
      request.post({
       headers:headers, 
       url:'http://localhost:3000/api/bicicletas/create',
       body: bici
    },function(error,response,body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(1).color).toBe("verde");
            done();
        });

      
     
    });
  });