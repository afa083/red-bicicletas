var Bicicleta = require('../../../models/bicicleta');
beforeEach(function() {
    Bicicleta.allbicis =[];
  });
  
beforeEach(function() { 
    console.log( "testeando…"); });

describe("Bicicleta.allbicis", function() {
    it("El array allbicis comienza vacio", function() {
      expect(Bicicleta.allbicis.length).toBe(0);
    });
  });

describe("Bicicleta.add", function() {
    it("Agrega elementos a el array", function() {
      expect(Bicicleta.allbicis.length).toBe(0);
      var a =  new Bicicleta (1 ,'verde', 'Todoterreno',[3.61068, -76.27099]);
      Bicicleta.add(a);
      expect(Bicicleta.allbicis.length).toBe(1);
      expect(Bicicleta.allbicis[0]).toBe(a);
    });
  });

  describe("Bicicleta.findById", function() {
    it("Devuelve le bici con el id :1", function() {

    var a =  new Bicicleta (1 ,'verde', 'Todoterreno');
    var b =  new Bicicleta (2 ,'rojo', 'urbana');
    Bicicleta.add(a);
    Bicicleta.add(b);
    var target = new Bicicleta.findById(1);
      expect(target.id).toBe(1);
      expect(target.color).toBe(a.color);
      expect(target.modelo).toBe(a.modelo);
    });
  });